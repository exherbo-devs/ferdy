# Copyright 2008 Fernando J. Pereda
# Copyright 2014 Niels Ole Salscheider
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PNV} suffix=tar.xz ] cmake

SUMMARY="Strictly RFC 3986 compliant URI parsing library written in C"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
            x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
        )
    test:
        dev-cpp/gtest[>=1.8.0]
"

src_configure() {
    local cmakeargs=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
        -DURIPARSER_ENABLE_INSTALL:BOOL=TRUE
        -DURIPARSER_BUILD_CHAR:BOOL=TRUE
        -DURIPARSER_BUILD_WCHAR_T:BOOL=TRUE
        -DURIPARSER_BUILD_DOCS:BOOL=$(option doc TRUE FALSE)
        -DURIPARSER_BUILD_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DURIPARSER_BUILD_TOOLS:BOOL=TRUE
        -DURIPARSER_WARNINGS_AS_ERRORS:BOOL=FALSE
    )

    ecmake "${cmakeargs[@]}"
}

